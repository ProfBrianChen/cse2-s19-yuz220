//////////////
//// CSE 02_HW07 Program2. StringAnalysis
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.util.*;
import java.lang.*;

public class StringAnalysis{
  //main method
  public static void main(String args[]){
    Scanner scan = new Scanner(System.in);
    
    System.out.println("Please enter a string: ");
    String stringName = scan.next();
    
    System.out.println("Please enter a position: ");
    int i  = scan.nextInt();
    
    System.out.println("String containing only characters : " + StringAnalysis.charCheck(stringName));
    System.out.println("String containing only characters at position: " + StringAnalysis.pos_charCheck(stringName,i));
  } //End of main method
  
  //Method to check whether the input is a character
  static boolean charCheck(String stringName){
    for(int i = 0;i < stringName.length();i++){
      if(Character.isLetter(stringName.charAt(i))){
        return true;
        }
      else{
        return false;
      }
      }
    return true;
    } //End of charactercheck method
  
  static boolean pos_charCheck(String stringName,int j){
     int i = 0;
     j = 0;
     
     if(j < stringName.length()){
       for(i = 0;i < j;i++){
         if(Character.isLetter(stringName.charAt(i))){
           } else
           return false;
         }
       }
     
     else{
       for(i = 0;i < stringName.length();i++){
         if(Character.isLetter(stringName.charAt(i))){
           } else{
           return false;
           }
         }
       }
     return true;
     } //End of the position method
}

