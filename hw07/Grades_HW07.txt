Homework 7
Total: 85

35: Program 1 compiles
Compiles!!!
+5: properly accepts input shapes, rejects anything other than rectangle, triangle, or circle
Checks for proper input
+5: Properly queries the user for shape parameters for each of the possible shapes
Proper queries
+5: correctly calculates areas.
Good calculations

35: Program 2 Compiles
Compiles!!
+5: one overloaded method properly checks if all characters in the string are letters.
Only checks first char. Both isLetter if statements return something meaning it leaves the method immediately. -5
+5: the other overloaded method correctly checks if all characters up to a given integer are letters
Always returns true due to indexing errors. -5
+5: checks that all inputs are valid.
Doesn't check if user input an int. -5