//////////////
//// CSE 02_HW07 Program1. Area
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.util.*;

public class Area{
  //main method
  public static void main(String args[]){
    
    int i = 0;
    String shape;
    double length,width,radius,base,height;
    
    //Ask for the shape users need to calculate
    System.out.print("Enter a shape(rectangle or triangle or circle) :");
    Scanner scan = new Scanner(System.in);
    
    do{
      i = 0;
      shape = scan.next();
      
      if(shape.equals("rectangle")){
        System.out.print("Enter a length : ");
        length = scan.nextDouble();
        System.out.print("Enter a width : ");
        width = scan.nextDouble();
        
        if(length >= 0 && width >= 0){
          Area.rectangle(length,width);
          }else{
          System.out.println("\nInvalid inputs.please retry");
          }
        }
      
      else if(shape.equals("triangle")){
        System.out.print("Enter a height : ");
        height = scan.nextDouble();
        System.out.print("Enter a base : ");
        base = scan.nextDouble();
        
        if(height >= 0 && base >= 0){
          Area.triangle(height,base);
          }else{
          System.out.println("\nInvalid inputs.please retry");
          }
        }
      
      else if(shape.equals("circle")){
        System.out.print("Enter a radius : ");
        radius = scan.nextDouble();
        if(radius >= 0 ){
          Area.circle(radius);
          }else{
          System.out.println("Invalid inputs.please retry");
          }
      }
       
       else{
         System.out.print("Enter valid shape (rectangle or triangle or circle) : ");
         i = 1;
         }
       } while (i != 0);
    
     } //End of main method
  
  
   public static void rectangle(double length,double width){
       System.out.println("Area of the rectangle : " + length*width); //print out the area of rectangle
   }
   public static void triangle(double height,double base){
       System.out.println("Area of the triangle : " + 0.5*height*base); //print out the area of triangle
   }
   public static void circle(double radius){
       System.out.println("Area of the circle : " + 3.14*radius*radius); //print out the area of circle
   }
}
