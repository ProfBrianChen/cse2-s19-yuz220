//////////////
//// CSE 02_HW05
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.util.Scanner;//import scanner method

//main class
public class hw05{
  
  //main method
  public static void main (String args[]){
    Scanner myScanner=new Scanner(System.in);
    boolean courseNum=false;
    
    while(courseNum == false){
      //ask user to type in number
      System.out.println("What is your course number?");
      courseNum = myScanner.hasNextInt();
      
      if (courseNum == true){//start of if statement
        int courseNumCheck = myScanner.nextInt();//identify integer
      }
      else{
        myScanner.next();
        System.out.println("You need to type in an integer!!!");
      }
    }
   
    boolean departNum=false;
    while(departNum==false){
      System.out.println("What is the department of that course?");
      departNum=myScanner.hasNext();
      if (departNum==true){
        String departNumCheck=myScanner.next();
      }
      else{
        myScanner.next();
      }
    }
    
    boolean timeNum=false;
    while(timeNum==false){
      
      System.out.println("How many times you meet every week?");
      courseNum=myScanner.hasNextInt();
      
      if (timeNum==true){
        int timeNumCheck=myScanner.nextInt();
      }
      else{
        myScanner.next();
        System.out.println("You need to type in an integer!!!");
      }
    }
    
    boolean startTime=false;
    while(startTime==false){
      
      System.out.println("What time does your class start?");
      startTime=myScanner.hasNext();
      if (startTime==true){
        String startTimeCheck=myScanner.next();
      }
      else{
        myScanner.next();
      }
    }
    
    boolean instruName=false;
    while(instruName==false){
      System.out.println("What is instructor's name?");
      instruName=myScanner.hasNext();
      if (instruName==true){
        String instruNameCheck=myScanner.next();
      }
      else{
        myScanner.next();
      }
    }
    
    boolean studentNum=false;
    while(studentNum==false){
      System.out.println("How many stydents in your class?");
      studentNum=myScanner.hasNextInt();
      if (studentNum==true){
        int studentNumCheck=myScanner.nextInt();
      }
      else{
        myScanner.next();
        System.out.println("You need to type in an integer!!!");
      } 
    }
  }
}