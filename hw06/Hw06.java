//////////////
//// CSE 02_HW06
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.util.Scanner; //import scanner method

//main class
public class Hw06 {
  
  //main method
  public static void main(String[] args) {
    int height;
    int width;
    int size;
    int length;
    Scanner input = new Scanner(System.in);
    
    String error = "  Error: please type in an integer.";
    String input_str;
    
    //Check the input
    while (true) {
      //ask user to type in the height
      System.out.print("Input your desired height: ");
      input_str = input.next();
    
      try {
        height = Integer.parseInt(input_str);
        break;
        } catch (Exception ex) {
        System.out.println(error);
        }
      } 
  
   //Check the input
   while (true) {
     //ask user to type in the width
     System.out.print("Input your desired width: ");
     input_str = input.next();
   
     try {
       width = Integer.parseInt(input_str);
       break;
       } catch (Exception ex) {
       System.out.println(error);
       }
     }
    
    //Check the input
    while (true) {
      //ask user to type in the size
      System.out.print("Input square size: ");
      input_str = input.next();
      try {
        size = Integer.parseInt(input_str);
         break;
        } catch (Exception ex) {
        System.out.println(error);
        }
      }
    
    //Check the input
    while (true) {
      //ask user to type in the length
      System.out.print("Input edge length: ");
      input_str = input.next();
      try {
        length = Integer.parseInt(input_str);
        break;
        } catch (Exception ex) {
        System.out.println(error);
        }
    }
  
    //initialize the variables
    int len = size + length;
    int loc1 = size / 2 - 1;
    int loc2 = loc1 + 1;
    
    for (int i = 0; i < height; i++) { //for loop for the height 
      for (int j = 0; j < width; j++) { //for loop for the width
        
        if (i % len == 0 || (i - size + 1) % len == 0) {
          if (j % len == 0 || (j - size + 1) % len == 0) {
            System.out.print('#');
            } 
          else if (j % len >= 0 && j % len <= size - 1) {
            System.out.print('-');
            } 
          else {
            if (size == 1)
              System.out.print('-');
            else
              System.out.print(' ');
            }
          }
        
        else if (i % len > 0 && i % len <= size - 1) {
          if (j % len == 0 || (j - size + 1) % len == 0) {
            System.out.print('|');
            } 
          else if (j % len >= size && j % len <= len - 1) {
            if (i % 12 == loc1 || i % 12 == loc2) {
              System.out.print('-');
              } 
            else {
              System.out.print(' ');
              }
            }
          
          else {
            System.out.print(' ');
            }
          }
        
        else {
          if (j % 12 == loc1 || j % 12 == loc2) {
            System.out.print('|');
            }
          else {
            System.out.print(' ');
            }
          }
        } //end of for loop for the width
   
      System.out.print('\n');
      } //end of for loop for the height
    
    }
  }
