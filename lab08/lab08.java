import java.util.Arrays;
import java.util.Random;
import java.math.*;


public class lab08{
  public static void main(String args[]) {
    
//     Random r = new Random();
//     int num1 = r.nextInt((100 - 50) + 1) + 50;
    int num1 = (int)(Math.random() * (100 - 50)) + 50;
    int[] randNum1;
    randNum1 = new int[num1];
    System.out.printf("The size of the array is: %d\n", num1);
    
    
    for(int i=0; i<num1; i++){
      int num2 = r.nextInt((99 - 0) + 1);
      randNum1[i] = num2;
    }
    
    System.out.println("The range of this array is: "+getRange(randNum1));
    System.out.println("The mean of this array is: "+getMean(randNum1));
    System.out.println("The standard deviation of this array is: "+getStdDev(randNum1));
    shuffle(randNum1);
    
  }
  
  public static int getRange(int [] array) {
    int min = array[0];
    int max = array[0];
    int range;
    
    Arrays.sort(array);
    System.out.println("The new order of this array is: ");
    for (int i = 0; i < array.length; i++) {
       System.out.printf("Random Num inserted @ %d is %d\n", i, array[i]);
    }
    
    for(int k=1; k<array.length; k++) {
      if(array[k] < min) {
        min = array[k];
      }
      
      if(array[k] > max) {
        max = array[k];
      }
      //Arrays.sort(array[i]);
    
    }
    range = max - min;
    return range;
  }
  
  public static double getMean(int [] array) {
    double sum = 0;
    double mean;
    
    for(int i=0; i<array.length; i++) {
      sum += i;
    }
    mean = sum/array.length;
    return mean;
  }
 
  public static double getStdDev(int [] array) {
    double standard = 0;
    double sum = 0;
    double temp2 = 0;
    double temp = 0;
    double std = 0;
    
    for(int i=0; i<array.length; i++) {
      temp = (array[i]-getMean(array))*(array[i]-getMean(array));
      sum += temp;
    }
    temp2 = temp/(array.length-1);
    standard = Math.sqrt(temp2);
    return standard;
  }
  
  public static void shuffle(int[] array) {
    
    int rnd1 = new Random().nextInt(array.length);
    int rnd2 = new Random().nextInt(array.length);
    
      int temp = array[rnd1];
      array[rnd1] = array[rnd2];
      array[rnd2] = temp;
    
    System.out.println("The first number picked at the position of " + rnd1 + " is " + array[rnd1]);
    System.out.println("The second number picked at the position of " + rnd2 + " is " + array[rnd2]);
    
    for(int i=0; i<array.length; i++) {
      System.out.printf("The number @ postition %d is %d\n", i, array[i]);
    }
    
    return;
    }
  
}


  
  




