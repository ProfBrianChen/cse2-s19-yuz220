import java.util.*;

public class letter {
   char arr[];

   letter(int n) {
       arr = new char[n];
       String str = "AMRSFGHIJKTUVWXbcdefghijklmnopqrNOPQstuvwBCDELxyYZaz";
       Random r = new Random();

       for (int i = 0; i < n; i++) {
           arr[i] = str.charAt(r.nextInt(52));
       }
   }

   public char[] getAtoM() {
       String str = "";
       for (int i = 0; i < arr.length; i++) {
           char c = Character.toUpperCase(arr[i]);
           if (c >= 'A' && c <= 'M') {
               str += arr[i];
           }
       }
       return str.toCharArray();
   }

   public char[] getNtoZ() {
       String str = "";
       for (int i = 0; i < arr.length; i++) {
           char c = Character.toUpperCase(arr[i]);
           if (c >= 'N' && c <= 'Z') {
               str += arr[i];
           }
       }
       return str.toCharArray();
   }


public class TestLetter {
   public static void main(String[] args) {
       letter l = new letter(14);
       System.out.println("Random character array: " + new String(l.arr));
       System.out.println("AtoM characters: " + new String(l.getAtoM()));
       System.out.println("NtoZ characters: " + new String(l.getNtoZ()));

   }
}
}