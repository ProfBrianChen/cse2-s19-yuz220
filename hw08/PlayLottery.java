//////////////
//// CSE 02_HW08 Program2. PlayLottery
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.util.*;

public class PlayLottery{
  
  //Main Method
  public static void main(String[] args) {
    Scanner scan =new Scanner(System.in);
    
    int i;
    int winning[] = numbersPicked();
    int userNums[] = new int[5];
    System.out.print("Enter 5 numbers between 0 and 59: ");
    
    for(i=0; i<5; i++) {
      userNums[i] = scan.nextInt();
    }
    
    System.out.println("Enter 5 numbers between 0 and 59: "+Arrays.toString(userNums));
    System.out.println("The winning numbers are: "+Arrays.toString(winning));
    
    if(userWins(userNums, winning)) {
      System.out.println("You win");
    }
    else {
      System.out.println("You lose");
    }
    
}

//Method of userWins
public static boolean userWins(int[] user, int[] winning){
  int i;
  for(i=0; i<5; i++){
    
    if(user[i]!=winning[i])
      return false;
    }
  
  return true;
  }

//Method of numbersPicked
public static int[] numbersPicked(){
  
  Random r = new Random();
  int []nums = new int[5];
  int n=0;
  
  while(n<5){
    int i;
    nums[n] = r.nextInt(59);
    boolean exists = false;
    
    for(i=0; i<n; i++)
      if(nums[i]==nums[n]){
        exists = true;
        break;
        }
    if(!exists)
      n++;
    }
  return nums;
  }

}
  
