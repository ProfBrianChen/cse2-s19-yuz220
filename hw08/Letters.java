//////////////
//// CSE 02_HW08 Program1. Letters
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.util.Random;

public class Letters {
   public static void main(String[] args) {
       
     Letter newLetter = new Letter(13);
     System.out.println("Random character array: " + new String(newLetter.arr));
     System.out.println("AtoM characters: " + new String(newLetter.getAtoM()));
     System.out.println("NtoZ characters: " + new String(newLetter.getNtoZ()));
   }
}

class Letter {
   char arr[];

  //Generate random character array
   Letter(int n) {
       arr = new char[n];
       String str = "KDSUONVOJEOMASObcdlinxkuDHSEINVinefdopqrNLhosbdinSEn";
       Random r = new Random();

       for (int i = 0; i < n; i++) {
           arr[i] = str.charAt(r.nextInt(52));
       }
   }

  // Array with letters from A to M
  public char[] getAtoM() {
    String str = "";
    
    for (int i = 0; i < arr.length; i++) {
      char c = Character.toUpperCase(arr[i]);
      
      if (c >= 'A' && c <= 'M') {
        str += arr[i];
        }
      }
    
    return str.toCharArray();
   }
  
  // Array with letters from N to Z
  public char[] getNtoZ() {
    String str = "";
    
    for (int i = 0; i < arr.length; i++) {
      char c = Character.toUpperCase(arr[i]);
      
      if (c >= 'N' && c <= 'Z') {
        str += arr[i];
        }
      }
    
    return str.toCharArray();
   }
}

