
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class Straight {
  
  public static int[] shuffle() {
    
	String[] suit = new String[] {"hearts","clubs","spades","diamonds"};
    String[] n = new String[] {"A","2","3","4","5","6","7","8","9","10","J","Q","K"};
    
    List<String> deck = new ArrayList<String>();
    for(String s:suit)
       for(String v:n)
          deck.add(s + v);
    System.out.println(deck);
    
    Random r = new Random();
    int array[] = new int[51];
    for(int j=0; j<100; j++) {
    	
    	int a = r.nextInt(51);
    	int b = r.nextInt(51);
    	
    	int temp = array[a];
    	array[a] = array[b];
    	array[b] = temp;
    	
    }
    return array;
  }
  
  
  public static int[] draw() {
    
	  int []array = shuffle();
	  int[]draw = new int[5];
	  int n=0;
	  
	  for(int i=0; i<5; i++) {
		  draw[i] = array[n];
		  n++;
	  }
	  
	  return draw;
  }

  public static int search(int arr[], int k) {
	  
	  Arrays.sort(arr);
	  int pivot = arr[k];
	  
	  for(int i=0; i<5; i++) {
		  if(arr[i]==pivot) {
			  return i;
		  }
	  }
	  return -1;
  }
  
  public static void straight() {
	  
  }


public static void main(String []args) {
	  
	  print(shuffle());
	  print(draw());
	  
	  
  }
  
  public static void print(int[] array) {
	  for(int i=0; i<array.length; i++) {
		  System.out.printf("%d \n", array[i]);
	  }
	  System.out.println();
  }
  
}

