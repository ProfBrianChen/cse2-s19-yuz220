//////////////
//// CSE 02_HW05
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.util.Scanner;//import scanner method

//main class
public class hw05{
  
  //main method
  public static void main (String args[]){
    Scanner myScanner=new Scanner(System.in);
    boolean ScourseNum=false;
    
    while(ScourseNum == false){
      //ask user to type in number
      System.out.println("What is your course number?");
      
      
      ScourseNum = myScanner.hasNextInt();
      if (ScourseNum == true){//start of if statement
        int IcourseNum=myScanner.nextInt();//identify integer
      }
      else{
        myScanner.next();
        System.out.println("You need to type in an integer!!!");
      }
    }
   
    boolean SdepartNum=false;
    while(SdepartNum==false){
      System.out.println("What is the department of that course?");
      SdepartNum=myScanner.hasNext();
      if (SdepartNum==true){
        String IdepartNum=myScanner.next();
      }
      else{
        myScanner.next();
      }
    }
    
    boolean StimeNum=false;
    while(StimeNum==false){
      System.out.println("How many times you meet every week?");
      ScourseNum=myScanner.hasNextInt();
      if (StimeNum==true){
        int ItimeNum=myScanner.nextInt();
      }
      else{
        myScanner.next();
        System.out.println("You need to type in an integer!!!");
      }
    }
    
    boolean SstartTime=false;
    while(SstartTime==false){
      System.out.println("What time does your class start?");
      SstartTime=myScanner.hasNext();
      if (SstartTime==true){
        String IstartTime=myScanner.next();
      }
      else{
        myScanner.next();
      }
    }
    
    boolean SinstruName=false;
    while(SinstruName==false){
      System.out.println("What is instructor's name?");
      SinstruName=myScanner.hasNext();
      if (SinstruName==true){
        String IinstruName=myScanner.next();
      }
      else{
        myScanner.next();
      }
    }
    boolean SstudentNum=false;
    while(SstudentNum==false){
      System.out.println("How many stydents in your class?");
      SstudentNum=myScanner.hasNextInt();
      if (SstudentNum==true){
        int IstudentNum=myScanner.nextInt();
      }
      else{
        myScanner.next();
        System.out.println("You need to type in an integer!!!");
      } 
    }
  }
}