
//main class
public class CardGenerator{
  //main method
  public static void main(String args[]){
  
    int Num=(int)(Math.random()*52+1);
    
    //initialize a variable
    String Identity="";
    
    ////Below is to find out the identity of the card
    switch( Num%13 ){
      case 1:
        Identity="Ace";
        break;
      case 11:
        Identity="Jack";
        break;
      case 12: 
        Identity="Queen";
        break;
      //all other numbers are fine to keep ther original number
      case 0:
        Identity="King";
        break;
      default:
        Identity=String.valueOf(Num%13);    
        break;
    }
    
    
    ////Below is  to find out the suit of the card
    
    //initial an variable
    String Suit="";
    
    if ( Num>=1 && Num<14 ){
       Suit = "Diamonds";
    }
    else if ( Num>=14&&Num<27 ){
       Suit ="Clubs";
    }
    else if ( Num>=27&&Num<40 ){
       Suit = "Hearts";
    }
    else if ( Num>=40&&Num<53 ){
       Suit = "Spades";
    }
    
    //print the output
    System.out.println("You picked the "+Identity+" of "+Suit);
    
  } 
} 