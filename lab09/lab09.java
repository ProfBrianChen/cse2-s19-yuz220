import java.util.Random;
import java.util.Scanner;

class lab09 {
  public static int binarySearch(int array[], int l, int r, int x) {
    if (r >= l) {
      int mid = 1+(r-1)/2;
      
      if (array[mid] == x) {
        return mid;
        }
      
      if (array[mid] > x) {
        return binarySearch(array, l, mid - 1, x);
      }
      
      return binarySearch(array, mid + 1, r, x);
      }
    return -1;
    }
  
  private static int linearSearch(int array[], int n, int temp) {
    for (int i = 0; i < n; i++) {
      if (array[i] == temp)
        return i;
      }
    
    return -1;
    }

   public static int[] fillRandomNumbers(int n) {
     int array[] = new int[n];
     Random r = new Random();
     
     for (int i = 0; i < n; i++) {
       array[i] = r.nextInt(n);
     }
  
     return array;
   }

   public static int[] fillRandomAscNumbers(int n) {
       int array[] = new int[n];
       Random r = new Random();
       int temp = n;
       
     for (int i = 0; i < n; i++) {
           temp = r.nextInt(n) + temp+1;
           array[i] = temp;
       }
     
     return array;
   }

   public static void main(String args[]) {
     Scanner sc = new Scanner(System.in);
     
     System.out.println("Enter 1 or 2 to choose: 1. Linear Search 2. Binary Search");
     int search = sc.nextInt();
     System.out.println("Enter the number of the elements: ");
       
     int n = sc.nextInt();
     int array[] = null;
     
     if (search == 1) {
       array = fillRandomNumbers(n);
       printArray(array);
       System.out.println("Enter a number to search");
       int temp = sc.nextInt();
       int pos = linearSearch(array, array.length, temp);
       
       if (pos != -1)
         System.out.println(temp + " found at : " + pos);
       
       
       else
         System.out.println(temp + " does not exist in array");
       
       } else {
       array = fillRandomAscNumbers(n);
       
       printArray(array);
       System.out.println("Enter number to search");
       int temp = sc.nextInt();
       int pos = binarySearch(array, 0, array.length, temp);
       
       if (pos != -1)
         System.out.println(temp + " found at : " + pos);
       else
         System.out.println(temp + " does not exist in array");
       }
     }
  
  private static void printArray(int[] aArr) {
    for (int i : aArr){
      System.out.print(i + " ");
    }
    
    System.out.println();
   }
}