import java.util.Scanner;
import java.util.Random;

public class lab07{
  
  //Random randomGenerator = new Random();
  public static String adj() {
    int n=(int)(Math.random()*10);
    
    switch(n){
      case 0:
        return "happy";
      case 1:
        return "interesting";
      case 2:
        return "calm";
      case 3:
        return "sad";
      case 4:
        return "crazy";
      case 5:
        return "strong";
      case 6:
        return "great";
      case 7:
        return "new";
      case 8:
        return "cool";
      case 9:
        return "expensive";
    }//end of switch
    
    return "";
  }
  
  public static String sub() {
    int n=(int)(Math.random()*10);
    
    switch(n){
      case 0:
        return "fox";
      case 1:
        return "rabbit";
      case 2:
        return "bear";
      case 3:
        return "shake";
      case 4:
        return "eagle";
      case 5:
        return "bat";
      case 6:
        return "squirrel";
      case 7:
        return "mouse";
      case 8:
        return "horse";
      case 9:
        return "cat";
    }//end of switch
    return "";
  }
  
  public static String verb() {
    int n=(int)(Math.random()*10);
    
    switch(n){
      case 0:
        return "made";
      case 1:
        return "said";
      case 2:
        return "round";
      case 3:
        return "gave";
      case 4:
        return "took";
      case 5:
        return "showed";
      case 6:
        return "ate";
      case 7:
        return "drank";
      case 8:
        return "watched";
      case 9:
        return "bought";
    }//end of switch
    
    return "";
  }
  
  public static String obj() {
    int n=(int)(Math.random()*10);
    
    switch(n){
      case 0:
        return "water";
      case 1:
        return "bread";
      case 2:
        return "flower";
      case 3:
        return "rice";
      case 4:
        return "football";
      case 5:
        return "apple";
      case 6:
        return "cheese";
      case 7:
        return "pencil";
      case 8:
        return "book";
      case 9:
        return "violin";
    }//end of switch
    
    return "";
  }
  
  //// Main Method
  public static void main(String args[]) {
    Scanner myScanner=new Scanner(System.in);
    int i=2;
    String adj="", sub="", verb="", obj="";
    
    while(i==2){
      adj = adj();
      sub = sub();
      verb = verb();
      obj = obj();
      
      System.out.println("The "+adj+" "+sub+" "+verb+" "+obj+".");
      
      System.out.println("Do you want this generated sentence to be your topic sentence?");
      System.out.print("Type 1 to keep this sentence.Type 2 to regenerate a sentence:");
      i = myScanner.nextInt();
      }
      
      System.out.println(metfirstSent(sub,obj,adj,verb));//call first sentence
      System.out.println(metsecondSent(sub));//call Second Sentence
      System.out.println(metthirdSent(sub));//call third sentence
      System.out.println(metforthSent(sub));//call forth sentence
    }// end of while loop
    
 
  public static String metfirstSent(String input_sub, String input_obj, String input_adj, String input_verb){
    String first;
    return first = "The " + input_adj + " " + input_sub+" " + input_verb + " " + input_obj + ".";
  }
  
  public static String metsecondSent(String input_sub){
    String second;
    return second = "This " + it(input_sub) + " was " + adj() + " " + verb() + " to " + obj() + ".";
  }
  
  public static String metthirdSent(String input_sub){
    String third;
    return third = "The " + it(input_sub) + " used " + obj() + " to " + verb() + " " + obj() + " .";
  }
  
  public static String metforthSent(String input_sub){
    String fourth;
    return fourth = "Finally, the "+it(input_sub)+" "+verb()+" with "+obj()+" .";
  }
  
  public static String it(String input_sub){
    String It;
    int i=(int)(Math.random()*2);
    if(i<1){
      return It="it";
    }
    else{
      return It=input_sub;
    }//end of it method
  }
}//end of class




