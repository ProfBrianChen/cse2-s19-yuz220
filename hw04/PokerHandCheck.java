//////////////
//// CSE 02_HW04 PokerHandCheck
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

public class PokerHandCheck {

    public static void main(String[] args) {
        // Scanner scan = new Scanner(System.in);

        // System.out.print("Please input the first card(0~51): ");
        int c1 = (int) (Math.random() * 52);
        // System.out.print("Please input the second card(0~51): ");
        int c2 = (int) (Math.random() * 52);
        // System.out.print("Please input the third card(0~51): ");
        int c3 = (int) (Math.random() * 52);
        // System.out.print("Please input the forth card(0~51): ");
        int c4 = (int) (Math.random() * 52);
        // System.out.print("Please input the fifth card(0~51): ");
        int c5 = (int) (Math.random() * 52);

        int t1 = c1 / 13, r1 = c1 % 13;
        int t2 = c2 / 13, r2 = c2 % 13;
        int t3 = c3 / 13, r3 = c3 % 13;
        int t4 = c4 / 13, r4 = c4 % 13;
        int t5 = c5 / 13, r5 = c5 % 13;

        System.out.println("Your random cards were:");
        System.out.print("  the ");
        switch(r1) {
            case 9:
                System.out.print("jack");
                break;
            case 10:
                System.out.print("queen");
                break;
            case 11:
                System.out.print("king");
                break;
            case 12:
                System.out.print("ace");
                break;
            default:
                System.out.print(r1+2);
                break;
        }
        System.out.print(" of ");
        switch(t1) {
            case 0:
                System.out.println("diamonds");
                break;
            case 1:
                System.out.println("clubs");
                break;
            case 2:
                System.out.println("hearts");
                break;
            case 3:
                System.out.println("spades");
                break;
        }

        System.out.print("  the ");
        switch(r2) {
            case 9:
                System.out.print("jack");
                break;
            case 10:
                System.out.print("queen");
                break;
            case 11:
                System.out.print("king");
                break;
            case 12:
                System.out.print("ace");
                break;
            default:
                System.out.print(r2+2);
                break;
        }
        System.out.print(" of ");
        switch(t2) {
            case 0:
                System.out.println("diamonds");
                break;
            case 1:
                System.out.println("clubs");
                break;
            case 2:
                System.out.println("hearts");
                break;
            case 3:
                System.out.println("spades");
                break;
        }

        System.out.print("  the ");
        switch(r3) {
            case 9:
                System.out.print("jack");
                break;
            case 10:
                System.out.print("queen");
                break;
            case 11:
                System.out.print("king");
                break;
            case 12:
                System.out.print("ace");
                break;
            default:
                System.out.print(r3+2);
                break;
        }
        System.out.print(" of ");
        switch(t3) {
            case 0:
                System.out.println("diamonds");
                break;
            case 1:
                System.out.println("clubs");
                break;
            case 2:
                System.out.println("hearts");
                break;
            case 3:
                System.out.println("spades");
                break;
        }

        System.out.print("  the ");
        switch(r4) {
            case 9:
                System.out.print("jack");
                break;
            case 10:
                System.out.print("queen");
                break;
            case 11:
                System.out.print("king");
                break;
            case 12:
                System.out.print("ace");
                break;
            default:
                System.out.print(r4+2);
                break;
        }
        System.out.print(" of ");
        switch(t4) {
            case 0:
                System.out.println("diamonds");
                break;
            case 1:
                System.out.println("clubs");
                break;
            case 2:
                System.out.println("hearts");
                break;
            case 3:
                System.out.println("spades");
                break;
        }

        System.out.print("  the ");
        switch(r5) {
            case 9:
                System.out.print("jack");
                break;
            case 10:
                System.out.print("queen");
                break;
            case 11:
                System.out.print("king");
                break;
            case 12:
                System.out.print("ace");
                break;
            default:
                System.out.print(r5+2);
                break;
        }
        System.out.print(" of ");
        switch(t5) {
            case 0:
                System.out.println("diamonds");
                break;
            case 1:
                System.out.println("clubs");
                break;
            case 2:
                System.out.println("hearts");
                break;
            case 3:
                System.out.println("spades");
                break;
        }


        System.out.println();

        int[] cnt = new int[13];
        cnt[r1] ++;
        cnt[r2] ++;
        cnt[r3] ++;
        cnt[r4] ++;
        cnt[r5] ++;

        int count2 = 0, count3 = 0;
        if (cnt[0] >= 3) count3 ++;
        else if (cnt[0] == 2) count2 ++;

        if (cnt[1] >= 3) count3 ++;
        else if (cnt[1] == 2) count2 ++;

        if (cnt[2] >= 3) count3 ++;
        else if (cnt[2] == 2) count2 ++;

        if (cnt[3] >= 3) count3 ++;
        else if (cnt[3] == 2) count2 ++;

        if (cnt[4] >= 3) count3 ++;
        else if (cnt[4] == 2) count2 ++;

        if (cnt[5] >= 3) count3 ++;
        else if (cnt[5] == 2) count2 ++;

        if (cnt[6] >= 3) count3 ++;
        else if (cnt[6] == 2) count2 ++;

        if (cnt[7] >= 3) count3 ++;
        else if (cnt[7] == 2) count2 ++;

        if (cnt[8] >= 3) count3 ++;
        else if (cnt[8] == 2) count2 ++;

        if (cnt[9] >= 3) count3 ++;
        else if (cnt[9] == 2) count2 ++;

        if (cnt[10] >= 3) count3 ++;
        else if (cnt[10] == 2) count2 ++;

        if (cnt[11] >= 3) count3 ++;
        else if (cnt[11] == 2) count2 ++;

        if (cnt[12] >= 3) count3 ++;
        else if (cnt[12] == 2) count2 ++;

        if (count3 > 0) {
            System.out.println("You have three of a kind!");
        } else if (count2 >= 2) {
            System.out.println("You have two pair!");
        } else if (count2 >= 1) {
            System.out.println("You have a pair!");
        } else {
            System.out.println("You have a high card hand!");
        }
    }
}
