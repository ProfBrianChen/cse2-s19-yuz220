//////////////
//// CSE 02_HW09
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

import java.io.*;
import java.util.*;

//public class
public class ArrayGames {
    private static Random r = new Random();
    
    //produce random array inputs, and generate your own random integer inputs
    public static int[] generate() {
        int size = r.nextInt(11) + 10;
        int[] ret = new int[size];
        for (int i = 0; i < size; i++) {
            ret[i] = r.nextInt(10);
        }
        return ret;
    }

    //print out the members of an integer array input
    public static void print(int[] values) {
        String repr = "{";
        for (int i = 0; i < values.length; i++) {
            repr += values[i];
            if (i == values.length - 1) repr += "}";
            else repr += ",";
        }
        System.out.print(repr);
    }
    
    //produce a new array long enough to contain both arrays
    public static int[] insert(int[] input1, int[] input2) {
        int l1 = input1.length;
        int pos = r.nextInt(l1);
        int[] res = new int[l1 + input2.length];
        for (int i = 0; i < pos; i++) {
            res[i] = input1[i];
        }
        for (int i = 0; i < input2.length; i++) {
            res[i + pos] = input2[i];
        }
        for (int i = pos; i < l1; i++) {
            res[input2.length + i] = input1[i];
        }
        return res;
    }
    
    //accept an input array and an input integer
    public static int[] shorten(int[] input, int val) {
        int size = 0;
        for (int i = 0; i < input.length; i++) {
            if (input[i] != val) size ++;
        }
        int[] ret = new int[size];
        for (int i = 0, j = 0; i < input.length; i++) {
            if (input[i] != val) ret[j ++] = input[i];
        }
        return ret;
    }

    //main method: asks the user if they want to run insert or shorten
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);

        System.out.println("1. generate");
        System.out.println("2. shorten");
        System.out.print("Please input the choice[1-2]: ");

        int cmd = scan.nextInt();
        if (cmd == 1) {
            int[] input1 = ArrayGames.generate();
            int[] input2 = ArrayGames.generate();
            System.out.print("Input1: ");
            ArrayGames.print(input1);
            System.out.print("\tInput2: ");
            ArrayGames.print(input2);
            System.out.println();

            int[] output = ArrayGames.insert(input1, input2);
            System.out.print("Output: ");
            ArrayGames.print(output);
            System.out.println();
        } else if (cmd == 2) {
            int[] input1 = ArrayGames.generate();
            int val = ArrayGames.r.nextInt(10);
            System.out.print("Input1: ");
            ArrayGames.print(input1);
            System.out.print("\tInput2: ");
            System.out.println(val);

            int[] output = ArrayGames.shorten(input1, val);
            System.out.print("Output: ");
            ArrayGames.print(output);
            System.out.println();
        } else {
            System.out.println("Wrong Option");
        }
    }
}