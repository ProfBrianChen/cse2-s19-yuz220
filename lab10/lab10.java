import java.util.Random;
import java.util.Scanner;

public class lab10{
  
  public static int[][] increasingMatrix(int width, int height, boolean format) {

		int matrix[][] = new int[width][height];

		int value = 1;

		if (format == true) {
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					matrix[i][j] = value;
					value++;
				}
			}
		}

		else if (format == false) {
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					matrix[j][i] = value;
					value++;
				}
			}
		}
		return matrix;
	}

	public static void printMatrix(int[][] array, boolean format) {

			for (int i = 0; i < array.length; i++) {
				for (int j = 0; j < array[i].length; j++) {
					System.out.printf("%d ", array[i][j]);
				}
				System.out.println();
			}


		if (array == null) {
			System.out.println("the array was empty!");
		}
	}

	public static int[][] translate(int[][] array) {

		int i = 0;
		int j = 0;
		int temp[][]= new int[array[0].length][array.length];
		
		for (i = 0; i < array[0].length; i++) {
			for (j = 0; j < array.length; j++) {
				
				temp[i][j] = array[j][i];
				
			}
		}

		return temp;
	}

	public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb) {

		
		int w = 0;
		int h = 0;
		
		if(formatb== false) {
			w=b[0].length;
			h=b.length;
		}
		
		if(formata == false) {
			w=a[0].length;
			h=a.length;
		}
		
		int matrix[][] = new int[w][h];
		//int trans[][] = new int[h][w];

//		if (a.length != b.length) {
//			System.out.println("the arrays cannot be added");
//			return null;
//		}

		 if (formata == true && formatb == true) {
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < b.length; j++) {
					matrix[i][j] = a[i][j] + b[i][j];
				}
			}
		}

		else if (formata == false && formatb == false) {
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < b.length; j++) {
					matrix[j][i] = a[j][i] + b[j][i];
				}
			}
		}

		else if (formata == false && formatb == true) {
			int transa[][] = translate(a);

			for (int i = 0; i < w; i++) {
				for (int j = 0; j < h; j++) {
					matrix[i][j] = transa[i][j] + b[i][j];
					
				}
			}
		}

		else if (formata == true && formatb == false) {
			int transb[][] = translate(b);
			
			
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[0].length; j++) {
					matrix[i][j] = a[i][j] + transb[i][j];					
				}
			}
		}
	
		return matrix;
	}

	public static void main(String[] args) {

		Random r = new Random();
		int width1 = r.nextInt(1+(5-1))+1;
		int width2 = r.nextInt(1+(5-1))+1;
		int height1 = r.nextInt(1+(5-1))+1;
		int height2 = r.nextInt(1+(5-1))+1;

		int[][] A = new int[width1][height1];
		int[][] B = new int[width1][height1];
		int[][] C = new int[width2][height2];

		A = increasingMatrix(width1, height1, true);
		B = increasingMatrix(width1, height1, false);
		C = increasingMatrix(width2, height2, true);

		int [][]addAB = addMatrix(A, true, B, false);
		int [][]addAC = addMatrix(A, true, C, true);

		System.out.println("Matrix A is: ");
		printMatrix(A, true);
		System.out.println();
		
		System.out.println("Matrix B is: ");
		printMatrix(B, false);
		System.out.println();
		
		System.out.println("Matrix C is: ");
		printMatrix(C, true);
		System.out.println();
		System.out.println("Matrix A+B is: ");
		printMatrix(addAB,true);  
		System.out.println();
		System.out.println("Matrix A+C is: ");
		printMatrix(addAC,true);  
		System.out.println();
		
		
	}
  
}


