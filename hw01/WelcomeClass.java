//////////////
//// CSE 02_HW01 Welcome Class
//// Yufei Zou
//// yuz220@lehigh.edu
//// Prof. Brian Chen

public class WelcomeClass{
  
  public static void main(String args[]) {
    
    ///prints all graphs and words to terminal window
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" /" + " \\" + "/" + " \\" + "/" + " \\" + "/" + " \\" + "/" + " \\" + "/" + " \\");
    System.out.println("<-Y--U--Z--2--2--0->");
    System.out.println(" \\" + " /" + "\\" + " /" + "\\" + " /" + "\\" + " /" + "\\" + " /" + "\\" + " /");
    System.out.println("  v  v  v  v  v  v");
    
    
  }
}