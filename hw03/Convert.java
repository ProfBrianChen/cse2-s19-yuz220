import java.util.Scanner;

public class Convert{
  // main method required for every Java program
  public static void main(String[] args) {
    

      Scanner myScanner = new Scanner( System.in );
      System.out.print("Enter the distance in meters in the form xx.xx: "); // Ask the user to enter the distance.
      double distance1 = myScanner.nextDouble();
    
      double inch = distance1 * 39.3700787; // Transfer meter to inch.
      System.out.println(distance1 + " meters is " + (String.format("%.4f", inch)) + " inches "); 
    
    
  }
}
  
