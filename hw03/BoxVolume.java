import java.util.Scanner;

public class BoxVolume{
  // main method required for every Java program
  public static void main(String[] args) {
    

      Scanner myScanner = new Scanner( System.in );
      
      System.out.print("Enter the width of the box: "); // Ask the user to enter the width of the box
      int width = myScanner.nextInt();
    
      System.out.print("Enter the length of the box: "); // Ask the user to enter the length of the box
      int length = myScanner.nextInt();
    
      System.out.print("Enter the height of the box: "); // Ask the user to enter the height of the box
      int height = myScanner.nextInt();
    
      int volume = length * width * height; // Calculate the volume through the values entered
   
      System.out.println("The volume inside the box is: " + volume); // Print the outcome
      
   
  }
}
  