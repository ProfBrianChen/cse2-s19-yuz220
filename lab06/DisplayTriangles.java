import java.util.Scanner;//declaration of scanner method
public class DisplayTriangles {
  public static void main(String args[]){
    Scanner myScanner= new Scanner(System.in);//declaration
    /*System.out.print("Choose an integer between 0 - 100: ");//prompt
    while(!myScanner.hasNextInt()){//use to judge integer
      System.out.print("Please type in an interger: ");//prompt of wrong input
      myScanner.next();
    }//end of the while loop used to judge integer*/
    int n=101;//used to start the while loop
    while(n<0||n>100){
      System.out.print("Please to type in an integer between 1-10: ");
      while(!myScanner.hasNextInt()){//use to judge integer
        System.out.print("Please type in an interger: ");//prompt of wrong input
        myScanner.next();
      }//end of the while loop used to judge integer
      n=myScanner.nextInt();
    }//end of the while loop used to judge range
    //System.out.println(n);//used to 
    
    //Pattern 1
    for(int i = 0; i < n ; i++) {    
    for(int j = 0; j <= i; j++) {  
      System.out.print(j+1);
    }
    //System.out.print(j+1);
    System.out.print("\n");
  }
    
    System.out.print("\r\n");
    
    //Pattern 2
    for(int i = n; i > 0; i--) {    
    for(int j = 1; j <= i ; j++) {
      System.out.print(j);
    }
    System.out.print("\n");
  }
    System.out.print("\r\n");
    
    //Pattern 3
    for(int i = 1; i <= n ; i++) { 
    for(int k = n; k > i; k--) {
      System.out.print(" "); 
    }
    for(int j = i; j>0; j--) {
      System.out.print(j);
    }
    System.out.print("\n");
  }
    System.out.print("\r\n");
    
    //Pattern 4
    for(int i = 1; i <= n ; i++) {
    for(int j = (n-i+1); j > 0; j--) {
      System.out.print(j);
    }
    System.out.print("\n");
  }
    
  }//end of main method
}//end of main class