public class Arithmetic{

     public static void main(String []args){
        
        int numPants = 3; //Number of pairs of pants
        double pantsPrice = 34.98; //Cost per pair of pants
        double totalCostOfPants; //total cost of pants

        int numShirts = 2; //Number of sweatshirts
        double shirtPrice = 24.99; //Cost per shirt
        double totalCostOfShirts; //total cost of shirts

        int numBelts = 1; //Number of belts
        double beltCost = 33.99; //cost per belt
        double totalCostOfBelts; //total cost of belts

        double paSalesTax = 0.06; //the tax rate
        
        totalCostOfPants = numPants * pantsPrice; //Total cost of pants
        double taxPants = totalCostOfPants * paSalesTax; //Tax for pants
        
        totalCostOfShirts = numShirts * shirtPrice; //Total cost of shirts
        double taxShirts = totalCostOfShirts * paSalesTax; //Tax for shirts
        
        totalCostOfBelts = numBelts * beltCost; //Total cost of belts
        double taxBelts = totalCostOfBelts * paSalesTax; //Tax for belts
        
        double totalCost_noTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; //Total cost of purchases (before tax)
        
        double totalSalesTax = taxPants + taxShirts + taxBelts; //Total sales tax
        
        double totalCost_incTax = totalSalesTax + totalCost_noTax; //Total paid for this transaction, including sales tax. 
        
        // Display the cost of each of item type
        System.out.println("The total cost of pants is " + totalCostOfPants);
        System.out.println("The total cost of shirts is " + totalCostOfShirts);
        System.out.println("The total cost of belts is " + totalCostOfBelts);
        
        // Display the total sales tax paid for buying the certain item
        System.out.println("The total sales tax paid for pants is " + (String.format("%.2f", taxPants)));
        System.out.println("The total sales tax paid for shirts is " + (String.format("%.2f", taxShirts)));
        System.out.println("The total sales tax paid for belts is " + (String.format("%.2f", taxBelts)));
        
        // Display the total cost of the purchases (before tax)
        System.out.println("The total cost of the purchases (before tax) is " + (String.format("%.2f", totalCost_noTax)));
        
        // Display the total sales tax
        System.out.println("The total sales tax is " + (String.format("%.2f", totalSalesTax)));
       
        // Display the total cost of the purchases (including sales tax)
        System.out.println("The total cost of the purchasese (including sales tax) is " + (String.format("%.2f", totalCost_incTax)));

     }
}